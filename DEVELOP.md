*created api folder for save all methodes of providers and their routes and export files in sms.js

*create auth.js for checking token and redirect to setting page

*create axios.js for axiosInstance and error handling

*create helper.js for rules function and date handling function

*/Sent/Messages.vue
. optionsFn function for limit dateRange
. rangeNumberCheck function check value between min & max
. onSearch function check diff between two date and create Notify and filter data with dateRange

*/Sent/SentForm.vue
. addReceiver function save unique value in selectedContacts

*/store/sms/mutations.js
. SET_TOKEN fuction have dispatch for refresh token and account data
. UPDATE_MESSAGES function get data and update status of messagee.
.SET_MESSAGE add message in 0 index of messages


*install VuexPersistence plugin for save data in state vuex

*install jsdoc plugin for descrip code generation

*quasar.config.js
. add boot/<namefile.js> in boot array 
. add css default file in css array
. add fontPack in extras array or uncommented
. change build config like vueRouterMode or rtl status
. add devServer for http server or https and port name
. change framework.lang for config
. 

*set config for deploy project on vercel
. add distDir: "dist" in quasar.config.js
. add vercel.json in root 