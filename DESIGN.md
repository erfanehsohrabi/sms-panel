*create project with quasar framework

q-date component customization:
1. ایجاد بازه  زمانی
2. ایجاد محدودیت برای نمایش تاریخ ها
3.  پشتیبانی از تقویم شمسی
4. اعمال ماسک برای نمایش تاریخ


q-table component customization:
1.دسترسی به تمامی سطرها و ستون ها بصورت مجزا


q-skeleton component customization:
1.ایجاد لودینگ با طرح مشخص و حالت های آماده

Loading component:
این کامپوننت برای لودینگ کلی صفحه است و کاملا با css طراحی شده.

داکیومنت همه ی کامپوننت ها در سایت زیر با مثال های مشخص و توضیحات کامل وجود دارد.
https://quasar.dev/

