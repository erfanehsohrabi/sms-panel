import kavenegar from "./smsProvider/kevenegar";
var providers = { kavenegar };

export default providers[localStorage.getItem("provider") || "kavenegar"];
