import { axiosInstance } from "boot/axios";
const CONFIG_ROUTE = "/account/config.json";
const UPDATE_MESSAGES_ROUTE = "/sms/status.json";
const MESSAGE_ROUTE = "/sms/send.json";
const BALANCE_ROUTE = "/account/info.json";

function setConfig(params) {
  return axiosInstance.get(CONFIG_ROUTE, { params });
}
function updateMessages(params) {
  return axiosInstance.get(UPDATE_MESSAGES_ROUTE, { params });
}
function sendMessage(params) {
  return axiosInstance.get(MESSAGE_ROUTE, { params });
}
function fetchBalance() {
  return axiosInstance.get(BALANCE_ROUTE);
}
export default {
  setConfig,
  updateMessages,
  fetchBalance,
  sendMessage,
};
