export default ({ Vue }) => {
  Vue.use(require("vue-jalali-moment"));
  Vue.prototype.$helper = {
    date: {
      jalaliToUnix(jalali) {
        if (!jalali) return null;
        return Vue.prototype.$moment(jalali, "jYYYY-jMM-jDD").unix() || "";
      },
      jalaliToUnixFull(jalali) {
        if (!jalali) return null;
        return (
          Vue.prototype.$moment(jalali, "jYYYY-jMM-jDD HH:mm").unix() || ""
        );
      },
      toShamsiFull(unixTime) {
        return Vue.prototype
          .$moment(unixTime, "X")
          .utcOffset(0)
          .format("jYYYY/jMM/jDD HH:mm");
      },
    },
    rules: {
      required: (value) =>
        (value !== "" && value !== null) || "پرکردن این فیلد اجباری است.",
      number: (value) => !isNaN(value) || "کاراکتر غیرمجاز (فقط عدد)",
      phone: (val) => /^0/.test(val) || "شماره باید با صفر شروع شود.",
      min11: (value) =>
        value.length == 11 || "شماره تلفن باید 11 کاراکتر باشد.",
      date: (val) => {
        if (val != "") {
          return (
            Vue.prototype.$moment(val, "jYYYY/jMM/jDD").isValid() ||
            "تاریخ نامعتبر"
          );
        }
      },
    },
  };
};
