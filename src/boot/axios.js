import axios from "axios";
import { Notify } from "quasar";
const KAVENEGAR_URL = "https://api.kavenegar.com/v1/";

var axiosInstance = axios.create({
  baseURL: KAVENEGAR_URL + localStorage.getItem("TOKEN"),
});

export default ({ Vue }) => {
  axiosInstance.interceptors.response.use(
    (response) => {
      if (response.data.message) {
        Notify.create({
          message: response.data.message,
          color: "green",
          html: true,
        });
      }
      return response;
    },
    (error) => {
      if (!error.response) {
        Notify.create({
          message: "مشکل شبکه لطفا اتصالات رو چک کنید",
          color: "warning",
          icon: "signal_wifi_off",
          position: "top",
        });
      } else {
        Notify.create({
          message: error.response.data.message,
          color: "red",
          icon: "chat_bubble",
          html: true,
        });
      }
      return Promise.reject(error);
    }
  );
  Vue.prototype.$axios = axiosInstance;
};

export { axiosInstance, KAVENEGAR_URL };
