import { Notify } from "quasar";
/**
 * check router change for userToken and redirect to setting page if false return
 */
export default ({ router, store, Vue }) => {
  router.beforeEach((to, from, next) => {
    if (
      !store.getters["sms/getToken"] &&
      !store.getters["sms/getToken"].length > 0 &&
      !(to.path == "/setting")
    ) {
      Notify.create({
        message: "برای ادامه لطفا توکن خود را وارد کنید",
        color: "red",
        icon: "token",
      });
      next({
        name: "setting", // back to safety route //
        query: { redirectFrom: to.fullPath },
      });
    }

    next();
  });
};
