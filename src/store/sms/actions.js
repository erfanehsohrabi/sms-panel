import sms from "../../api/sms";

export function setConfig({ commit }, params) {
  return sms
    .setConfig(params)
    .then(({ data }) => {
      if (data.return.status == 200) {
        commit("SET_CONFIG", data.entries);
      }
    })
    .catch((error) => {
      console.log(error);
    });
}
export function updateMyMessages({ commit }, params) {
  return new Promise((resolve, reject) => {
    sms
      .updateMessages(params)
      .then(({ data }) => {
        if (data.return.status == 200) {
          commit("UPDATE_MESSAGES", data.entries);
          resolve(data.entries);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}
export function sendMessage({ commit }, params) {
  return new Promise((resolve, reject) => {
    sms
      .sendMessage(params)
      .then(({ data }) => {
        if (data.return.status == 200) {
          commit("SET_MESSAGE", data.entries[0]);
          resolve(data.entries);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}
export function fetchBalance({ commit }, params) {
  return sms
    .fetchBalance()
    .then(({ data }) => {
      if (data.return.status == 200) {
        commit("SET_BALANCE", data.entries.remaincredit);
      }
    })
    .catch((error) => {
      console.log(error);
    });
}
