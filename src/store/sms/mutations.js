import { axiosInstance, KAVENEGAR_URL } from "boot/axios";

export function SET_TOKEN(state, token) {
  if (state.token != token) {
    localStorage.setItem("TOKEN", token);
    axiosInstance.defaults.baseURL = KAVENEGAR_URL + token;
    state.token = token;
    //refresh account
    this.dispatch("sms/fetchBalance");
    this.dispatch("sms/setConfig");
  }
}
export function SET_CONFIG(state, config) {
  state.config = config;
}
export function UPDATE_MESSAGES(state, messages) {
  var temp = state.messages;
  temp.map((message) => {
    var indexItem = 0;
    indexItem = messages.findIndex((x) => {
      return message.messageid == x.messageid;
    });
    message.status = messages[indexItem].status;
    message.statustext = messages[indexItem].statustext;
  });
  state.messages = temp;
}
export function SET_MESSAGE(state, message) {
  state.messages.splice(0, 0, message);
}
export function DELETE_DELIVERED_MESSAGES(state) {
  var temp = state.messages.filter((message) => {
    return message.status != 10;
  });
  state.messages = temp;
}
export function SET_CONTACT(state, contact) {
  state.contacts = [...new Set(state.contacts).add(contact)];
}
export function SET_BALANCE(state, balance) {
  state.balance = balance;
}
