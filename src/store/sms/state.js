export default function () {
  return {
    balance: 0,
    token: localStorage.getItem("TOKEN") || "",
    config: {},
    messages: [],
    contacts: [
      {
        id: 1,
        name: "علی احمدی",
        phone: "09923142008",
      },
      {
        id: 2,
        name: "سارا محمدی",
        phone: "09923142009",
      },
      {
        id: 3,
        name: "احمد علوی",
        phone: "09923142006",
      },
      {
        id: 4,
        name: "سحر رضایی",
        phone: "09923142007",
      },
    ],
  };
}
