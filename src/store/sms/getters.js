export default {
  getToken(state) {
    return state.token;
  },
  getMyConfig(state) {
    return state.config;
  },
  getMyMessages(state) {
    return state.messages;
  },
  getMyContact(state) {
    return state.contacts;
  },
  getBalance(state) {
    return state.balance;
  },
  getContactByPhone: (state) => (phone) => {
    return state.contacts.find((e) => e.phone == phone) || false;
  },
};
