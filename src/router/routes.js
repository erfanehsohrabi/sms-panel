const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        name: "home",
        meta: {
          name: "پیشخوان",
        },
        path: "",
        component: () => import("pages/Index.vue"),
      },
      {
        name: "setting",
        meta: {
          name: "تنظیمات",
        },
        path: "setting",
        component: () => import("pages/setting.vue"),
      },
      {
        meta: {
          name: "پیام های دریافتی",
        },
        name: "messagesReceives",
        path: "/messages/receives",
        component: () => import("pages/messages/receives.vue"),
      },
      {
        meta: {
          name: "پیام های ارسالی",
        },
        name: "messagesSent",
        path: "/messages/sent",
        component: () => import("pages/messages/sent.vue"),
      },
      {
        meta: {
          name: "ایجاد پیام",
        },
        name: "messageCreate",
        path: "/messages/create",
        component: () => import("src/pages/messages/create.vue"),
      },
      {
        meta: {
          name: "مخاطبین",
        },
        name: "contacts",
        path: "/contacts",
        component: () => import("pages/contacts/index.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
